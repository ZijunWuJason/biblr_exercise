from setuptools import setup

setup(
    name='biblr',
    packages=['biblr'],
    include_package_data=True,
    install_requires=[
        'flask',
        'requests',
    ],
)

from setuptools import setup

setup(
    name='flaskr',
    packages=['flaskr'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)