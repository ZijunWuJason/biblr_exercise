import os
from biblr import biblr
import unittest
import tempfile

class BiblrTestCase(unittest.TestCase):

        def setUp(self):
            self.db_fd, biblr.app.config['DATABASE'] = tempfile.mkstemp()
            biblr.app.testing = True
            self.app = biblr.app.test_client()
            with biblr.app.app_context():
                biblr.init_db()

        def tearDown(self):
            os.close(self.db_fd)
            os.unlink(biblr.app.config['DATABASE'])

if __name__ == '__main__':
    unittest.main()
